# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Original source: https://opendev.org/openstack/reno

from reno.formatter import _indent_for_list


def format_report(loader, config, versions_to_include, title=None,
                  show_source=True, branch=None):
    report = []
    if title:
        report.append("# %s" % title)
        report.append('')

    # Read all of the notes files.
    file_contents = {}
    for version in versions_to_include:
        for filename, sha in loader[version]:
            body = loader.parse_note_file(filename, sha)
            file_contents[filename] = body

    for version in versions_to_include:
        if '-' in version:
            # This looks like an "unreleased version".
            version_title = config.unreleased_version_title or version
        else:
            version_title = version
        report.append('')
        report.append("## %s" % version_title)
        report.append('')

        if config.add_release_date:
            report.append('Release Date: ' + loader.get_version_date(version))
            report.append('')

        # Add the preludes.
        notefiles = loader[version]
        prelude_name = config.prelude_section_name
        notefiles_with_prelude = [(n, sha) for n, sha in notefiles
                                  if prelude_name in file_contents[n]]
        if notefiles_with_prelude:
            prelude_title = prelude_name.replace('_', ' ').title()
            report.append('')
            report.append("#### %s" % prelude_title)
            report.append('')

        for n, sha in notefiles_with_prelude:
            if show_source:
                report.append('<!-- %s @ %s -->\n' % (n, sha))
            report.append(file_contents[n][prelude_name])
            report.append('')

        # Add other sections.
        for section_name, section_title in config.sections:
            notes = [
                (n, fn, sha)
                for fn, sha in notefiles
                if file_contents[fn].get(section_name)
                for n in file_contents[fn].get(section_name, [])
            ]
            if notes:
                report.append('')
                report.append("#### %s" % section_title)
                report.append('')
                for n, fn, sha in notes:
                    if show_source:
                        report.append('<!-- %s @ %s -->\n' % (fn, sha))
                    report.append('- %s' % _indent_for_list(n))
                report.append('')

    return '\n'.join(report)
