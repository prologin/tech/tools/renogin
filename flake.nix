{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    futils.url = "github:numtide/flake-utils";
    poetry2nix.url = "github:nix-community/poetry2nix";
  };

  outputs = { self, nixpkgs, futils, poetry2nix } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (lib) recursiveUpdate;
      inherit (futils.lib) eachDefaultSystem defaultSystems;

      nixpkgsFor = lib.genAttrs defaultSystems (system: import nixpkgs {
        inherit system;
        overlays = [
          poetry2nix.overlay
          self.overlay
        ];
      });

      poetryArgs = { pkgs, groups ? [ ] }: {
        projectDir = self;
        src = self;
        inherit groups;

        meta = with lib; {
          inherit (self) description;
          maintainers = with maintainers; [ risson ];
        };
      };

      anySystemOutputs = {
        overlay = final: prev: {
          renogin = final.poetry2nix.mkPoetryApplication (poetryArgs { pkgs = final; });
        };
      };

      multipleSystemsOutputs = eachDefaultSystem (system:
        let
          pkgs = nixpkgsFor.${system};
        in
        {
          devShell = pkgs.mkShell {
            buildInputs = with pkgs; [
              git
              poetry
              (pkgs.poetry2nix.mkPoetryEnv (removeAttrs (poetryArgs { inherit pkgs; groups = [ "dev" ]; }) [ "meta" "src" "propagatedBuildInputs" ]))
            ];
          };

          packages = {
            inherit (pkgs) renogin;
            default = pkgs.renogin;
          };

          apps = {
            renogin = {
              type = "app";
              program = "${self.packages.${system}.renogin}/bin/renogin";
            };
            default = {
              type = "app";
              program = "${self.packages.${system}.default}/bin/renogin";
            };
          };
        });
    in
    recursiveUpdate multipleSystemsOutputs anySystemOutputs;
}
